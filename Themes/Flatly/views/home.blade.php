{{-- @extends('layouts.master')

@section('title')
    {{ $page->title }} | @parent
@stop
@section('meta')
    <meta name="title" content="{{ $page->meta_title}}" />
    <meta name="description" content="{{ $page->meta_description }}" />
@stop

@section('content')
    <div class="row">
        <h1>{{ $page->title }}</h1>
        {!! $page->body !!}
    </div>
@stop --}}
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>QuickSmart</title>
		<link href="https://simerr.une.edu.au/quicksmart/css/colourScheme.css" rel="stylesheet" type="text/css">
		<link href="https://simerr.une.edu.au/quicksmart/css/layout.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://simerr.une.edu.au/quicksmart/css/print.css" type="text/css" media="print" />
		<link rel="shortcut icon" type="image/x-icon" href="https://www.une.edu.au/simerr/quicksmart/images/favicon.ico">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
    <style type="text/css">
    #homeleft{
          position: relative;
          max-width: 540px;
          min-height: 727px;
          padding: 20px;
          padding-top:10px;
          background-color: rgba(255,255,255,0.85);
          -webkit-border-bottom-left-radius: 10px;
          -moz-border-radius-bottomleft: 10px;
          border-bottom-left-radius: 10px;
        }
        #content{
          background-image: url('https://simerr.une.edu.au/quicksmart/images/trees.jpg');
          background-position: right top;
          background-repeat: no-repeat;
          min-height: 757px;
          padding: 0px;

        }
        .feeditem{
          border: solid 1px #9E9E9E;
          padding: 10px;
          padding-top:0px;
          padding-bottom: 0px;
          margin-bottom: 10px;
        }
        .feeditem h1{
          margin-top: 4px;
        }
        p{
          font-size: 14px;
        }
        .feeditem h1 a{
          text-decoration: none;
        }
        .feeditem div{
          clear: both;
          font-size: 10px;
          text-align: right;
        }
        .feeditem div p{
          margin: 0px;
          font-size: 10px;
        }
        .feedthumb{
          max-width: 90px;
          max-height: 90px;
          float: left;
          margin-right: 10px;
        }
        /*Ribbon*/
        #ribbon a{
          background:#593184;
          color:#fff;
          text-decoration:none;
          font-family:arial, sans-serif;
          text-align:center;
          font-weight:bold;
          padding:5px 40px;
          font-size:2rem;
          line-height:2rem;
          position:absolute;
          top:0px;
          left: 0px;
          transition:0.3s;
        }
        #ribbon a:hover{
          background:#8dc63f;
          color:#fff;
        }
        #ribbon a::before,#ribbon a::after{
          content:"";
          width:100%;
          display:block;
          position:absolute;
          top:1px;
          left:0;
          height:1px;
          background:#fff;
        }
        #ribbon a::after{
          bottom:1px;
          top:auto;
        }

        #ribbon, #ribbon a{
          width:500px;
        }

        @media screen and (min-width:700px){
          #ribbon{
            position:absolute;
            display:block;
            top:0;
            right:0;
            width:250px;
            overflow:hidden;
            height:250px;
          }
          #ribbon a{
            width:245px;
            position:absolute;
            top:50px;
            right:-60px;
            transform:rotate(45deg);
            -webkit-transform:rotate(45deg);
            box-shadow:10px 0px 20px rgba(0,0,0,1);

            }
          }
          @media screen and (max-width:500px){
            #homeleft{
              padding-top:40px;
            }
            #ribbon, #ribbon a{
              left:0px;
              width:100%;
            }

          }

          #sociallinks {
            display: inline-flex;
            list-style-type: none;
          }
    </style>
    <script>
    		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    		  ga('create', 'UA-13206888-1', 'auto');
    		  ga('send', 'pageview');

    		</script>

		{{-- <style>
			img{
				border: none;
			}
			#wrapper{

				border: solid 1px #000;
			}
			.submenu{
				background-color: #000;
			}
		</style> --}}

	</head>
	<body style="width:60%;margin-left: 20%;">
    {!! $page->body !!}
  </body>
</html>
