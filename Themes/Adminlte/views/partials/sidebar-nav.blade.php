<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar sidebar-offcanvas">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        {!! $sidebar !!}

  <!-- logout -->
        <ul data-widget="tree" class="sidebar-menu tree">
        <li class="clearfix">
          <a href="{{ route('logout') }}">
            <i class="fa fa-sign-out"></i>
            <span>{{ trans('core::core.general.sign out') }}</span>
          </a>
        </li>
        </ul>
  <!-- /.logout -->
  
    </section>
    <!-- /.sidebar -->
</aside>
