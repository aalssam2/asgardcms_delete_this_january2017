<?php

namespace Modules\QSODashboards\Repositories\Eloquent;

use Modules\QSODashboards\Repositories\DashboardsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDashboardsRepository extends EloquentBaseRepository implements DashboardsRepository
{
}
