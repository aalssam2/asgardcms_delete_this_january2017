<?php

namespace Modules\QSODashboards\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface DashboardsRepository extends BaseRepository
{
}
