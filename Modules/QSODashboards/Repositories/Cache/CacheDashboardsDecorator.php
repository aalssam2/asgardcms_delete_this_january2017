<?php

namespace Modules\QSODashboards\Repositories\Cache;

use Modules\QSODashboards\Repositories\DashboardsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDashboardsDecorator extends BaseCacheDecorator implements DashboardsRepository
{
    public function __construct(DashboardsRepository $dashboards)
    {
        parent::__construct();
        $this->entityName = 'qsodashboards.dashboards';
        $this->repository = $dashboards;
    }
}
