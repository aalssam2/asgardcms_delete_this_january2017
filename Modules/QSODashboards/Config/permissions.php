<?php

return [
    // 'qsodashboards.dashboards' => [
    //     'index' => 'qsodashboards::dashboards.list resource',
    //     'create' => 'qsodashboards::dashboards.create resource',
    //     'edit' => 'qsodashboards::dashboards.edit resource',
    //     'destroy' => 'qsodashboards::dashboards.destroy resource',
    // ],

    'admindashboard' => [
      'index' => 'qsodashboards::dashboards.admin.list resource',
      'create' => 'qsodashboards::dashboards.admin.create resource',
      'edit' => 'qsodashboards::dashboards.admin.edit resource',
      'destroy' => 'qsodashboards::dashboards.admin.destroy resource',
    ],
    'learnerdashboard' => [
      'index' => 'qsodashboards::dashboards.learner.list resource',
      'create' => 'qsodashboards::dashboards.learner.create resource',
      'edit' => 'qsodashboards::dashboards.learner.edit resource',
      'destroy' => 'qsodashboards::dashboards.learner.destroy resource',
    ],

];
