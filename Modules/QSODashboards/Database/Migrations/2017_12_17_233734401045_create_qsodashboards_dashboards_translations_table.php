<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQSODashboardsDashboardsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qsodashboards__dashboards_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('dashboards_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['dashboards_id', 'locale']);
            $table->foreign('dashboards_id')->references('id')->on('qsodashboards__dashboards')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qsodashboards__dashboards_translations', function (Blueprint $table) {
            $table->dropForeign(['dashboards_id']);
        });
        Schema::dropIfExists('qsodashboards__dashboards_translations');
    }
}
