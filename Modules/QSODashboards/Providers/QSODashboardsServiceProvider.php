<?php

namespace Modules\QSODashboards\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\QSODashboards\Events\Handlers\RegisterQSODashboardsSidebar;

class QSODashboardsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterQSODashboardsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('dashboards', array_dot(trans('qsodashboards::dashboards')));
            // append translations

        });
    }

    public function boot()
    {
        $this->publishConfig('qsodashboards', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\QSODashboards\Repositories\DashboardsRepository',
            function () {
                $repository = new \Modules\QSODashboards\Repositories\Eloquent\EloquentDashboardsRepository(new \Modules\QSODashboards\Entities\Dashboards());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\QSODashboards\Repositories\Cache\CacheDashboardsDecorator($repository);
            }
        );
// add bindings

    }
}
