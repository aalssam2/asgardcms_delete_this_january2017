<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->get('admin', [
    'as' => 'admindashboard.index',
    'uses' => 'DashboardsController@admin',
    'middleware' => 'can:admindashboard.index',
]);


$router->get('learner', [
    'as' => 'learnerdashboard.index',
    'uses' => 'DashboardsController@learner',
    'middleware' => 'can:learnerdashboard.index',
]);


// $router->group(['prefix' =>'/qsodashboards'], function (Router $router) {
//     $router->bind('dashboards', function ($id) {
//         return app('Modules\QSODashboards\Repositories\DashboardsRepository')->find($id);
//     });
//     $router->get('dashboards', [
//         'as' => 'admin.qsodashboards.dashboards.index',
//         'uses' => 'DashboardsController@index',
//         'middleware' => 'can:qsodashboards.dashboards.index'
//     ]);
//     $router->get('dashboards/create', [
//         'as' => 'admin.qsodashboards.dashboards.create',
//         'uses' => 'DashboardsController@create',
//         'middleware' => 'can:qsodashboards.dashboards.create'
//     ]);
//     $router->post('dashboards', [
//         'as' => 'admin.qsodashboards.dashboards.store',
//         'uses' => 'DashboardsController@store',
//         'middleware' => 'can:qsodashboards.dashboards.create'
//     ]);
//     $router->get('dashboards/{dashboards}/edit', [
//         'as' => 'admin.qsodashboards.dashboards.edit',
//         'uses' => 'DashboardsController@edit',
//         'middleware' => 'can:qsodashboards.dashboards.edit'
//     ]);
//     $router->put('dashboards/{dashboards}', [
//         'as' => 'admin.qsodashboards.dashboards.update',
//         'uses' => 'DashboardsController@update',
//         'middleware' => 'can:qsodashboards.dashboards.edit'
//     ]);
//     $router->delete('dashboards/{dashboards}', [
//         'as' => 'admin.qsodashboards.dashboards.destroy',
//         'uses' => 'DashboardsController@destroy',
//         'middleware' => 'can:qsodashboards.dashboards.destroy'
//     ]);
// // append
//
// });
