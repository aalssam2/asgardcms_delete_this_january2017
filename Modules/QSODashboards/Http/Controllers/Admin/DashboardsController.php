<?php

namespace Modules\QSODashboards\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\QSODashboards\Entities\Dashboards;
use Modules\QSODashboards\Http\Requests\CreateDashboardsRequest;
use Modules\QSODashboards\Http\Requests\UpdateDashboardsRequest;
use Modules\QSODashboards\Repositories\DashboardsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class DashboardsController extends AdminBaseController
{
    /**
     * @var DashboardsRepository
     */
    private $dashboards;

    public function __construct(DashboardsRepository $dashboards)
    {
        parent::__construct();

        $this->dashboards = $dashboards;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$dashboards = $this->dashboards->all();

        return view('qsodashboards::admin.dashboards.index', compact(''));
    }

    /**
     * Display the dashboard with its widgets
     * @return \Illuminate\View\View
     */
    public function admin()
    {


        return view('qsodashboards::admin.dashboard', compact('customWidgets'));
    }

    /**
     * Display the dashboard with its widgets
     * @return \Illuminate\View\View
     */
    public function learner()
    {


        return view('qsodashboards::admin.learnerdashboard', compact('customWidgets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('qsodashboards::admin.dashboards.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateDashboardsRequest $request
     * @return Response
     */
    public function store(CreateDashboardsRequest $request)
    {
        $this->dashboards->create($request->all());

        return redirect()->route('admin.qsodashboards.dashboards.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('qsodashboards::dashboards.title.dashboards')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Dashboards $dashboards
     * @return Response
     */
    public function edit(Dashboards $dashboards)
    {
        return view('qsodashboards::admin.dashboards.edit', compact('dashboards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Dashboards $dashboards
     * @param  UpdateDashboardsRequest $request
     * @return Response
     */
    public function update(Dashboards $dashboards, UpdateDashboardsRequest $request)
    {
        $this->dashboards->update($dashboards, $request->all());

        return redirect()->route('admin.qsodashboards.dashboards.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('qsodashboards::dashboards.title.dashboards')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Dashboards $dashboards
     * @return Response
     */
    public function destroy(Dashboards $dashboards)
    {
        $this->dashboards->destroy($dashboards);

        return redirect()->route('admin.qsodashboards.dashboards.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('qsodashboards::dashboards.title.dashboards')]));
    }
}
