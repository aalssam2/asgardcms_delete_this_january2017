<?php

return [
  'admin'=>[
    'list resource' => 'List dashboards',
    'create resource' => 'Create dashboards',
    'edit resource' => 'Edit dashboards',
    'destroy resource' => 'Destroy dashboards',
    'title' => [
        'dashboards' => 'Admin Dashboard',
        'create dashboards' => 'Create a dashboards',
        'edit dashboards' => 'Edit a dashboards',
    ],
    'button' => [
        'create dashboards' => 'Create a dashboards',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
  ],

  'learner'=>[
    'list resource' => 'List dashboards',
    'create resource' => 'Create dashboards',
    'edit resource' => 'Edit dashboards',
    'destroy resource' => 'Destroy dashboards',
    'title' => [
        'dashboards' => 'Learner Dashboard',
        'create dashboards' => 'Create a dashboards',
        'edit dashboards' => 'Edit a dashboards',
    ],
    'button' => [
        'create dashboards' => 'Create a dashboards',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
  ],

];
