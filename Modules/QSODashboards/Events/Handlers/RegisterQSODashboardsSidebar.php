<?php

namespace Modules\QSODashboards\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterQSODashboardsSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('dashboard::dashboard.name'), function (Group $group) {
          $group->weight(1);
          $group->hideHeading();
            $group->item(trans('qsodashboards::dashboards.admin.title.dashboards'), function (Item $item) {
              $item->icon('fa fa-dashboard');
              $item->route('admindashboard.index');
              $item->isActiveWhen(route('admindashboard.index', null, false));
              $item->authorize(
                  $this->auth->hasAccess('admindashboard.index')
              );
          });
          $group->item(trans('qsodashboards::dashboards.learner.title.dashboards'), function (Item $item) {
            $item->icon('fa fa-dashboard');
            $item->route('learnerdashboard.index');
            $item->isActiveWhen(route('learnerdashboard.index', null, false));
            $item->authorize(
                $this->auth->hasAccess('learnerdashboard.index')
            );
        });

        });

        return $menu;
    }
}
