<?php

namespace Modules\QSODashboards\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Dashboards extends Model
{
    use Translatable;

    protected $table = 'qsodashboards__dashboards';
    public $translatedAttributes = [];
    protected $fillable = [];
}
