<?php

namespace Modules\QSODashboards\Entities;

use Illuminate\Database\Eloquent\Model;

class DashboardsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'qsodashboards__dashboards_translations';
}
