<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLearnerDashboardWidgetsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learnerdashboard__widgets_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('widgets_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['widgets_id', 'locale']);
            $table->foreign('widgets_id')->references('id')->on('learnerdashboard__widgets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('learnerdashboard__widgets_translations', function (Blueprint $table) {
            $table->dropForeign(['widgets_id']);
        });
        Schema::dropIfExists('learnerdashboard__widgets_translations');
    }
}
