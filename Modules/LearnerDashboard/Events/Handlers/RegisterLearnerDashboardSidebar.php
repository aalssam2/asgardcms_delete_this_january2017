<?php

namespace Modules\LearnerDashboard\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterLearnerDashboardSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.learner'), function (Group $group) {
          $group->weight(1);
          $group->authorize(
              $this->auth->hasAccess('learnerdashboard.sidebar.group')
          );
                $group->item(trans('learnerdashboard::widgets.title.widgets'), function (Item $item) {

                    $item->icon('fa fa-gamepad');
                    $item->weight(100);
                    $item->route('admin.learnerdashboard.widgets.index');
                    $item->authorize(
                        $this->auth->hasAccess('learnerdashboard.widgets.index')
                    );
                });
// append

            });


        return $menu;
    }
}
