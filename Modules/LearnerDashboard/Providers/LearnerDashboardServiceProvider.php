<?php

namespace Modules\LearnerDashboard\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\LearnerDashboard\Events\Handlers\RegisterLearnerDashboardSidebar;

class LearnerDashboardServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterLearnerDashboardSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('widgets', array_dot(trans('learnerdashboard::widgets')));
            // append translations

        });
    }

    public function boot()
    {
        $this->publishConfig('learnerdashboard', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\LearnerDashboard\Repositories\WidgetsRepository',
            function () {
                $repository = new \Modules\LearnerDashboard\Repositories\Eloquent\EloquentWidgetsRepository(new \Modules\LearnerDashboard\Entities\Widgets());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\LearnerDashboard\Repositories\Cache\CacheWidgetsDecorator($repository);
            }
        );
// add bindings

    }
}
