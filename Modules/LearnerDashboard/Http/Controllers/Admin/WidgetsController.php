<?php

namespace Modules\LearnerDashboard\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\LearnerDashboard\Entities\Widgets;
use Modules\LearnerDashboard\Http\Requests\CreateWidgetsRequest;
use Modules\LearnerDashboard\Http\Requests\UpdateWidgetsRequest;
use Modules\LearnerDashboard\Repositories\WidgetsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class WidgetsController extends AdminBaseController
{
    /**
     * @var WidgetsRepository
     */
    private $widgets;

    public function __construct(WidgetsRepository $widgets)
    {
        parent::__construct();

        $this->widgets = $widgets;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$widgets = $this->widgets->all();

        return view('learnerdashboard::admin.widgets.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('learnerdashboard::admin.widgets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateWidgetsRequest $request
     * @return Response
     */
    public function store(CreateWidgetsRequest $request)
    {
        $this->widgets->create($request->all());

        return redirect()->route('admin.learnerdashboard.widgets.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('learnerdashboard::widgets.title.widgets')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Widgets $widgets
     * @return Response
     */
    public function edit(Widgets $widgets)
    {
        return view('learnerdashboard::admin.widgets.edit', compact('widgets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Widgets $widgets
     * @param  UpdateWidgetsRequest $request
     * @return Response
     */
    public function update(Widgets $widgets, UpdateWidgetsRequest $request)
    {
        $this->widgets->update($widgets, $request->all());

        return redirect()->route('admin.learnerdashboard.widgets.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('learnerdashboard::widgets.title.widgets')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Widgets $widgets
     * @return Response
     */
    public function destroy(Widgets $widgets)
    {
        $this->widgets->destroy($widgets);

        return redirect()->route('admin.learnerdashboard.widgets.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('learnerdashboard::widgets.title.widgets')]));
    }
}
