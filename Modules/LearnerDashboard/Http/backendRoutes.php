<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/learnerdashboard'], function (Router $router) {
    $router->bind('widgets', function ($id) {
        return app('Modules\LearnerDashboard\Repositories\WidgetsRepository')->find($id);
    });
    $router->get('widgets', [
        'as' => 'admin.learnerdashboard.widgets.index',
        'uses' => 'WidgetsController@index',
        'middleware' => 'can:learnerdashboard.widgets.index'
    ]);
    $router->get('widgets/create', [
        'as' => 'admin.learnerdashboard.widgets.create',
        'uses' => 'WidgetsController@create',
        'middleware' => 'can:learnerdashboard.widgets.create'
    ]);
    $router->post('widgets', [
        'as' => 'admin.learnerdashboard.widgets.store',
        'uses' => 'WidgetsController@store',
        'middleware' => 'can:learnerdashboard.widgets.create'
    ]);
    $router->get('widgets/{widgets}/edit', [
        'as' => 'admin.learnerdashboard.widgets.edit',
        'uses' => 'WidgetsController@edit',
        'middleware' => 'can:learnerdashboard.widgets.edit'
    ]);
    $router->put('widgets/{widgets}', [
        'as' => 'admin.learnerdashboard.widgets.update',
        'uses' => 'WidgetsController@update',
        'middleware' => 'can:learnerdashboard.widgets.edit'
    ]);
    $router->delete('widgets/{widgets}', [
        'as' => 'admin.learnerdashboard.widgets.destroy',
        'uses' => 'WidgetsController@destroy',
        'middleware' => 'can:learnerdashboard.widgets.destroy'
    ]);
// append

});
