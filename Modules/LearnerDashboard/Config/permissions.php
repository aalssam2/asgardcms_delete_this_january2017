<?php

return [
    'learnerdashboard.widgets' => [
        'index' => 'learnerdashboard::widgets.list resource',
        'create' => 'learnerdashboard::widgets.create resource',
        'edit' => 'learnerdashboard::widgets.edit resource',
        'destroy' => 'learnerdashboard::widgets.destroy resource',
    ],
    'learnerdashboard.sidebar' => [
        'group' => 'learnerdashboard::sidebar.show group',
    ],
// append

];
