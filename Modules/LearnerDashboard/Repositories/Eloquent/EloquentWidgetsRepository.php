<?php

namespace Modules\LearnerDashboard\Repositories\Eloquent;

use Modules\LearnerDashboard\Repositories\WidgetsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentWidgetsRepository extends EloquentBaseRepository implements WidgetsRepository
{
}
