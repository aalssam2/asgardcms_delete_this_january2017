<?php

namespace Modules\LearnerDashboard\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface WidgetsRepository extends BaseRepository
{
}
