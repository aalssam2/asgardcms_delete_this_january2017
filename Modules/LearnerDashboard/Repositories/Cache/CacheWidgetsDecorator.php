<?php

namespace Modules\LearnerDashboard\Repositories\Cache;

use Modules\LearnerDashboard\Repositories\WidgetsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheWidgetsDecorator extends BaseCacheDecorator implements WidgetsRepository
{
    public function __construct(WidgetsRepository $widgets)
    {
        parent::__construct();
        $this->entityName = 'learnerdashboard.widgets';
        $this->repository = $widgets;
    }
}
