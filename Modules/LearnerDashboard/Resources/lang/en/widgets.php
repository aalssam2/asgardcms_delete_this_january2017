<?php

return [
    'list resource' => 'List widgets',
    'create resource' => 'Create widgets',
    'edit resource' => 'Edit widgets',
    'destroy resource' => 'Destroy widgets',
    'title' => [
        'widgets' => 'Component One',
        'create widgets' => 'Create a widgets',
        'edit widgets' => 'Edit a widgets',
    ],
    'button' => [
        'create widgets' => 'Create a widgets',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
