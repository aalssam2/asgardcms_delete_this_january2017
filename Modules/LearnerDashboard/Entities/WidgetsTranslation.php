<?php

namespace Modules\LearnerDashboard\Entities;

use Illuminate\Database\Eloquent\Model;

class WidgetsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'learnerdashboard__widgets_translations';
}
