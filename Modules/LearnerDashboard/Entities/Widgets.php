<?php

namespace Modules\LearnerDashboard\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Widgets extends Model
{
    use Translatable;

    protected $table = 'learnerdashboard__widgets';
    public $translatedAttributes = [];
    protected $fillable = [];
}
