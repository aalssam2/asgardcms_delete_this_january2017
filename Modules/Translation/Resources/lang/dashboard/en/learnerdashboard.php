<?php

return [
    'name' => 'Learner dashboard',
    'edit grid' => 'Edit grid',
    'reset grid' => 'Reset grid',
    'save grid' => 'Save grid',
    'add widget' => 'Add widget',
    'add widget to learner dashboard' => 'Add widget to learner dashboard',
    'reset not needed' => 'Learner dashboard didn\'t need resetting',
    'learner dashboard reset' => 'Learner dashboard has been reset',
    'list resource' => 'View learner dashboard',
    'edit resource' => 'Update learner dashboard',
    'reset resource' => 'Reset learner dashboard',
    'welcome-title' => 'Welcome title',
    'welcome-description' => 'Welcome description',
    'welcome-enabled' => 'Enable welcome box?',
    'configure your website' => 'Configure your website',
    'add pages' => 'Add pages',
    'add menus' => 'Add menus',
];
