<?php

return [
    'dashboard' => [
        'index' => 'dashboard::dashboard.list resource',
        'update' => 'dashboard::dashboard.edit resource',
        'reset' => 'dashboard::dashboard.reset resource',
    ],
    'learnerdashboard' => [
        'index' => 'dashboard::learnerdashboard.list resource',
        'update' => 'dashboard::learnerdashboard.edit resource',
        'reset' => 'dashboard::learnerdashboard.reset resource',
    ],
];
